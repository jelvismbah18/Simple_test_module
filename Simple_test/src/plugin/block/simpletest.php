<?php


namespace Drupal\Simple_test\src\Controller;

 

use Drupal\Core\Controller\ControllerBase;

 

class SimpleController extends ControllerBase {

  public function content() {

    return array(

      '#type' => 'markup',

      '#markup' => t('Simple test'),

    );

  }

}

