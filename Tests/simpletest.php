<?php

namespace Drupal\simple_test_module\Tests;

use Drupal\Simple_test\WebTestBase;

class simpletest extends WebTestBase {
 
public static $modules = ['node', 'simple_test_module'];

public function testsimpletestRouterURLIsAccessible() {

    $this->drupalGet('simple_test_module/ask-user');

    $this->assertResponse('Simple test');
  }
}

public function testSimpletestSubmitButtonExists() {

    $this->drupalGet('simple_test_module/ask-user');

    $this->assertResponse('Simple test');

    $this->assertFieldById('edit-submit');
  }

public function testsimpletestFieldOptionsExist() {

    $this->drupalGet('simple_test_module/ask-user');

    $this->assertResponse('Simple test');

 $this->assertFieldByName('Simple_test');

$complete = ['simple.test.info.yml' 'simple.test.module.links.menu' 'simple.test.php''simple.test.routing.yml' ];

    foreach ($complete as $completed) {

      $this->assertOption('edit-Simple-test', $completed);
    }

 $this->assertNoOption('edit-Simple-test', 'New');

 }

public function testsimpletestSubmit() {

    $this->drupalPostForm(

      'simple_test_module/ask-user',

      array(
 'Simple_test' => 'Simple.test.php'

      ),

      t('Submit!')

    );
 $this->assertUrl('<front>');

    $this->assertText('Simple.test.php' 'Completed');

  }

